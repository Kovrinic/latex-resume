---
title: assets
layout: default
---

{% assign docs = site.static_files %}
{% assign global_dir_count = 0 %}
{% for doc in docs %}
  {% assign path_array = doc.path | split: "/" | size %}
  {% if path_array > 2 %}
    {% assign = global_dir_count = global_dir_count | plus: 1 %}
  {% endif %}
{% endfor %}

<header>
  <h1>
    <a href="">{{ site.baseurl }}</a>/
  </h1>
</header>
<main>
  <div class="meta">
    <div id="summary">
      <span class="meta-item"><b>{{ global_dir_count }}</b> directories</span>
      <span class="meta-item"><b>{{ docs.size | minus: global_dir_count }}</b> files</span>
      <span class="meta-item"><input type="text" placeholder="filter" id="filter" onkeyup='filter()'></span>
    </div>
  </div>
  <div class="listing">
    <table aria-describedby="summary">
      <thead>
        <tr>
          <th>Name</th>
          <th>Size</th>
          <th class="hideable">Modified</th>
        </tr>
      </thead>
      <tbody>
      {% for doc in docs %}
        {% assign dirs = doc.path | split: "/" %}
        {% assign dir_count = dirs | size %}
        <tr class="file">
          {% if dir_count > 2 %}
          <td>
            <a href="{{ sites.baseurl }}/{{ dirs[1] }}/">
              <svg width="1.5em" height="1em" version="1.1" viewBox="0 0 317 259">
                <use xlink:href="#folder"></use>
              </svg>
              <span class="name">{{ dirs[1] }}</span>
            </a>
          </td>
          <td data-order="-1">&mdash;</td>
          <td class="hideable"><time datetime="{{ doc.modified_time | time_tag }}">{{ doc.modified_time }}</time></td>
          {% else %}
          <td>
            <a href="{{ doc.path | prepend: site.baseurl }}">
              <svg width="1.5em" height="1em" version="1.1" viewBox="0 0 265 323">
                <use xlink:href="#file"></use>
              </svg>
              <span class="name">{{ doc.name }}</span>
            </a>
          </td>
          <td data-order="{{ doc.name | file_size }}">{{ doc.name | file_size: true }}</td>
          <td class="hideable"><time datetime="{{ doc.modified_time | time_tag }}">{{ doc.modified_time }}</time></td>
          {% endif %}
        </tr>
      {% endfor %}
      </tbody>
    </table>
  </div>
</main>

