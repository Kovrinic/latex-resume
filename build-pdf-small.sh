#!/bin/bash

# vars
RESUME_BASE_NAME="Resume_MatthewRothfuss"

# create PDF for resume site
# run twice, 1st to gather requried tex files, 2nd to generate bookmarks
texliveonfly --compiler=xelatex --terminal_only -a "-pdf -jobname=${RESUME_BASE_NAME} -interaction=nonstopmode" resume.tex
latexmk -pdf -jobname="${RESUME_BASE_NAME}" -e '$pdflatex=q/xelatex %O -interaction=nonstopmode %S/' resume.tex

# switch to Fontawesome 4, and create FA4 PDF
# run twice, 1st to gather requried tex files, 2nd to generate bookmarks
sed -i '/setboolean{boolfa5}{true}/ {s/^/%/}' resume.cls
texliveonfly --compiler=xelatex --terminal_only -a "-pdf -jobname=${RESUME_BASE_NAME}_FA4 -interaction=nonstopmode" resume.tex
latexmk -pdf -jobname="${RESUME_BASE_NAME}_FA4" -e '$pdflatex=q/xelatex %O -interaction=nonstopmode %S/' resume.tex

# convert PDFs into png images for Readme
convert -trim -density 150 -antialias "${RESUME_BASE_NAME}.pdf" -background white -resize 1024x -quality 100 -flatten -sharpen 0x1.0 "${RESUME_BASE_NAME}_FA5.png"
convert -trim -density 150 -antialias "${RESUME_BASE_NAME}_FA4.pdf" -background white -resize 1024x -quality 100 -flatten -sharpen 0x1.0 "${RESUME_BASE_NAME}_FA4.png"
