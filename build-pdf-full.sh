#!/bin/bash

# vars
RESUME_BASE_NAME="Resume_MatthewRothfuss"

# create PDF for resume site
latexmk -pdf -jobname="${RESUME_BASE_NAME}" -e '$pdflatex=q/xelatex %O -interaction=nonstopmode %S/' resume.tex

# switch to Fontawesome 4, and create FA4 PDF
sed -i '/setboolean{boolfa5}{true}/ {s/^/%/}' resume.cls
latexmk -pdf -jobname="${RESUME_BASE_NAME}_FA4" -e '$pdflatex=q/xelatex %O -interaction=nonstopmode %S/' resume.tex

# convert PDFs into png images for Readme
convert -trim -density 150 -antialias "${RESUME_BASE_NAME}.pdf" -background white -resize 1024x -quality 100 -flatten -sharpen 0x1.0 "${RESUME_BASE_NAME}_FA5.png"
convert -trim -density 150 -antialias "${RESUME_BASE_NAME}_FA4.pdf" -background white -resize 1024x -quality 100 -flatten -sharpen 0x1.0 "${RESUME_BASE_NAME}_FA4.png"
